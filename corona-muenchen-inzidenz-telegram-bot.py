"""
* Corona München Inzidenz Telegram Bot
* Copyright (C) 2021 Moritz Zwerger
*
* This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*
* This software is not affiliated with the city of munich, the RKI or the LGL or anybody else.
"""

import traceback
import urllib.parse
from datetime import datetime
from threading import Timer

import requests

import sources

SOURCES = [sources.MuenchenRKI(), sources.MuenchenFallzahlen(), sources.MuenchenCorona(), sources.MuenchenLGL()]
POLL_DELAY = 60 * 5
TELEGRAM_CHAT_ID = '@<Channel>'
TELEGRAM_ERROR_CHAT_ID = ''
TELEGRAM_BOT_TOKEN = '<Bot token>'
SOURCE_MAX_FAIL_COUNT = 3

# Limit messages to 1 per day
lastAnnouncement = datetime.today().date()


def sendTelegramMessage(chatId, message):
    telegramResponse = requests.get("https://api.telegram.org/bot" + TELEGRAM_BOT_TOKEN + "/sendMessage?chat_id=" + chatId + "&parse_mode=markdown&text=%s" % urllib.parse.quote(message)).json()
    if not telegramResponse["ok"]:
        raise Exception("Failed to send telegram message: %s" % telegramResponse["description"])


def getTrace(exception):
    return ''.join(traceback.format_exception(etype=type(exception), value=exception, tb=exception.__traceback__))


def fail(exception):
    print("Fetching failed with: %s" % exception)
    if TELEGRAM_ERROR_CHAT_ID == "":
        return
    try:
        sendTelegramMessage(TELEGRAM_ERROR_CHAT_ID, "Muenchen Inzidenz Bot Error: %s\n ```%s```" % (str(exception), getTrace(exception)))
    except Exception as newException:
        print("Failed to send a fail message!")
        print(getTrace(newException))


def formatFloat(number):
    return str(number).replace(".", ",")


def checkCurrentInzidenz():
    print("Fetching current inzidenz...")

    for source in SOURCES:
        if source.getFailCount() >= SOURCE_MAX_FAIL_COUNT:
            continue
        today = datetime.today().date()
        global lastAnnouncement
        try:
            currentInzidenz = source.fetchInzidenz()
            currentReproduktionszahl = -1
            try:
                currentReproduktionszahl = source.fetchReproduktionszahl()
            except NotImplementedError:
                # Data is not implemented, just skip
                pass
            print("Inzidenz (%s) is: %s, %s" % (source.getSourceName(), currentInzidenz, currentReproduktionszahl))

            if currentInzidenz != source.getLastInzidenz():
                source.setLastInzidenz(currentInzidenz)
                if today <= lastAnnouncement:
                    continue

                reproduktionszahlText = ""
                if currentReproduktionszahl != -1:
                    reproduktionszahlText = ", die Reproduktionszahl ist `%s`" % formatFloat(currentReproduktionszahl)

                announcementMessage = "Die 7-Tage-Inzidenz in München ist `%s`%s. Quelle: [%s](%s)" % (formatFloat(currentInzidenz), reproduktionszahlText, source.getSourceName(), source.getSourceURL())
                print(announcementMessage)
                sendTelegramMessage(TELEGRAM_CHAT_ID, announcementMessage)
                lastAnnouncement = today
        except Exception as exception:
            print(getTrace(exception))
            source.onFail()
            if source.getFailCount() >= SOURCE_MAX_FAIL_COUNT:
                print("Source %s failed %d times. It exceeds the maximum fail retry attempts of %d. Disabling it." % (source.getSourceName(), source.getFailCount(), SOURCE_MAX_FAIL_COUNT))
                fail(exception)
            else:
                print("Source %s failed %d times" % (source.getSourceName(), source.getFailCount()))


# fetch all sources once
checkCurrentInzidenz()


class PerpetualTimer:
    def __init__(self, delay, executor):
        self.delay = delay
        self.executorFunction = executor
        self.timer = Timer(self.delay, self.executor)

    def executor(self):
        self.executorFunction()
        self.timer = Timer(self.delay, self.executor)
        self.timer.start()

    def start(self):
        self.timer.start()

    def cancel(self):
        self.timer.cancel()


print("Starting polling every %d seconds." % POLL_DELAY)
timer = PerpetualTimer(POLL_DELAY, checkCurrentInzidenz)
timer.start()

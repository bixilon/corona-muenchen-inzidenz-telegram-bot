# Corona München Inzidenz Telegram Bot

Just a simple bot, that can send you (or a group) a telegram message, when the 7 day inzidenz of munich changed (Corona)

## Setup:

1. Create a Telegram channel and add your bot to it
2. Set `TELEGRAM_CHAT_ID` to your public channel id (with prefixed `@`)
3. Optional: Set `TELEGRAM_ERROR_CHAT_ID` to your telegram user id to receive errors
4. Set `TELEGRAM_BOT_TOKEN` to your bot token
5. Run `python3 corona-muenchen-inzidenz-telegram-bot.py` and enjoy.

### systemd

If you want to autostart this bot, create the file `/etc/systemd/system/corona-muenchen-inzidenz-telegram-bot.service`:

```
[Unit]
Description=corona-muenchen-inzidenz-telegram-bot

[Service]
User=root
WorkingDirectory=/root/corona-muenchen-inzidenz-telegram-bot
LimitNOFILE=4096
ExecStart=python3 corona-muenchen-inzidenz-telegram-bot.py
Restart=on-failure
StartLimitInterval=600

[Install]
WantedBy=multi-user.target
```

Adjust `WorkingDirectoy` to your needs.

Then run `systemctl enable --now corona-muenchen-inzidenz-telegram-bot` to enable the service and start it.

The bot pulls the current inzidenz every 5 minutes.

## Demo

See [@MuenchenInzidenz](https://t.me/MuenchenInzidenz)

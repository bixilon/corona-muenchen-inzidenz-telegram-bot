FROM python:3-slim
ADD corona-muenchen-inzidenz-telegram-bot.py /
ADD sources.py /
ADD requirements.txt /
RUN python3 -m pip install -r requirements.txt

CMD [ "python3", "./corona-muenchen-inzidenz-telegram-bot.py" ]

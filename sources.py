"""
* Corona München Inzidenz Telegram Bot
* Copyright (C) 2021 Moritz Zwerger
*
* This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*
* This software is not affiliated with the city of munich, the RKI or the LGL or anybody else.
"""
import random
import re
from abc import abstractmethod

import requests


class CoronaSources:
    lastData = 0.0
    failCount = 0

    @abstractmethod
    def fetchInzidenz(self):
        raise NotImplementedError

    @abstractmethod
    def getSourceURL(self):
        raise NotImplementedError

    @abstractmethod
    def getSourceName(self):
        raise NotImplementedError

    @abstractmethod
    def fetchReproduktionszahl(self):
        raise NotImplementedError

    def getLastInzidenz(self):
        return self.lastData

    def setLastInzidenz(self, lastData):
        self.lastData = lastData

    def getFailCount(self):
        return self.failCount

    def onFail(self):
        self.failCount = self.failCount + 1

    def resetFailCounter(self):
        self.failCount = 0


class MuenchenRKI(CoronaSources):
    def getSourceURL(self):
        return "https://corona.rki.de"

    def getSourceName(self):
        return "München RKI"

    def fetchInzidenz(self):
        jsonData = requests.get(
            "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/ArcGIS/rest/services/RKI_Landkreisdaten/FeatureServer/0/query?where=GEN%3D%27M%C3%BCnchen%27&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&resultType=none&distance=0.0&units=esriSRUnit_Meter&returnGeodetic=false&outFields=GEN%2Ccounty%2Ccases7_per_100k_txt&returnGeometry=false&returnCentroid=false&featureEncoding=esriDefault&multipatchOption=xyFootprint&maxAllowableOffset=&geometryPrecision=&outSR=&datumTransformation=&applyVCSProjection=false&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnQueryGeometry=false&returnDistinctValues=false&cacheHint=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&having=&resultOffset=&resultRecordCount=&returnZ=false&returnM=false&returnExceededLimitFeatures=false&quantizationParameters=&sqlFormat=none&f=pjson&token=").json()
        for data in jsonData["features"]:
            if data["attributes"]["county"] == "SK München":
                return float(data["attributes"]["cases7_per_100k_txt"].replace(",", "."))
        raise Exception("SK München not found")

    def fetchReproduktionszahl(self):
        raise NotImplementedError("The RKI does not provide this data.")


class MuenchenFallzahlen(CoronaSources):
    def getSourceURL(self):
        return "https://www.muenchen.de/rathaus/Stadtinfos/Coronavirus-Fallzahlen.html"

    def getSourceName(self):
        return "München-Fallzahlen"

    # Die 7-Tage-Inzidenz für München beträgt laut Landesamt für Gesundheit und Lebensmittelsicherheit (LGL) 116,4 (Stand 11.4.). Aufgrund eines Datenproblems zwischen LGL und RKI liegt der Wert des RKI nur bei 94,4.
    INZIDENZ_REGEX = re.compile(r"Die 7-Tage-Inzidenz für München beträgt( laut RKI| laut Landesamt für Gesundheit und Lebensmittelsicherheit \(LGL\))? (\d+(,\d+)?)")
    REPRODUKTIONSZAHL_REGEX = re.compile(r"Die Reproduktionszahl für München( laut RKI)? liegt bei (\d+(,\d+)?)")

    def fetchPage(self):
        return requests.get(self.getSourceURL()).content.decode('utf-8').replace("<strong>", "").replace("</strong>", "")

    def fetchNumber(self, regex):
        pattern = regex.search(self.fetchPage())
        if pattern is None:
            raise Exception("Regex does not match!")
        return float((pattern.group(2)).replace(",", "."))

    def fetchInzidenz(self):
        return self.fetchNumber(self.INZIDENZ_REGEX)

    def fetchReproduktionszahl(self):
        return self.fetchNumber(self.REPRODUKTIONSZAHL_REGEX)


class MuenchenCorona(MuenchenFallzahlen):
    def getSourceURL(self):
        return "https://www.muenchen.de/aktuell/2020-03/coronavirus-muenchen-infektion-aktueller-stand.html"

    def getSourceName(self):
        return "München-Corona"


class MuenchenLGL(CoronaSources):
    def getSourceURL(self):
        return "https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/"

    def getSourceName(self):
        return "LGL"

    INZIDENZ_REGEX = re.compile(r"<tr><td>MünchenStadt</td>(<td>[a-zA-Z+\-.,0-9)(]*</td>*){4}<td>(\d+(,\d+)?)</td>")

    def fetchInzidenz(self):
        html = requests.get(self.getSourceURL()).content.decode('utf-8').replace(" ", "").replace("\n", "").replace("\r", "")
        pattern = self.INZIDENZ_REGEX.search(html)
        if pattern is None:
            raise Exception("Regex does not match!")
        return float((pattern.group(2)).replace(",", "."))

    def fetchReproduktionszahl(self):
        raise NotImplementedError("The LGL does not provide this data.")


class DummySource(CoronaSources):
    def getSourceURL(self):
        return "If you see this url here, something id wrong here. Just contact the channel administrator."

    def getSourceName(self):
        return "Dummy"

    def fetchInzidenz(self):
        return random.randrange(10, 100)

    def fetchReproduktionszahl(self):
        return random.randrange(10, 100)
